# README
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Forex bot & backtest system with Python *by Bluefever Software*
This is the rewrite of **[Forex bot & backtest system with Python](https://www.youtube.com/watch?v=zKk2iuuNJO0&list=PLZ1QII7yudbecO6a-zAI6cuGP1LLnmW8e)**, by *[Bluefever Software](https://www.youtube.com/channel/UCFkfibjxPzrP0e2WIa8aJCg)*

###### Notes
1. This is a trading bot and backtester for the Forex market using the [OANDA API](https://developer.oanda.com/rest-live-v20/introduction/)

### How do I get set up?  
**Summary of set up**  
Use a virtual environment with *direnv* and *pyenv*. See [Python Virtual Environments made super-easy with direnv](https://blog.adam-uhlir.me/python-virtual-environments-made-super-easy-with-direnv-307611c3a49a)
